<?php

namespace App\DataTables;

use Modules\Students\Entities\Student;
use Yajra\DataTables\Services\DataTable;

class StudentsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', 'modules.students.action')
            ->rawColumns(['action'])
            ->addIndexColumn();
    }


    /**
     * @param Student $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Student $model)
    {
        return $model->newQuery()->select('id', 'name', 'aclass_id', 'gender', 'identifier_number', 'status', 'created_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addColumnBefore([
                'defaultContent' => '',
                'data' => 'DT_Row_Index',
                'name' => 'id',
                'title' => '#',
                'render' => null,
                'orderable' => true,
                'searchable' => false,
                'exportable' => false,
                'printable' => true,
                'footer' => '',
            ])
            ->minifiedAjax()
            ->parameters([
                'dom' => 'Blfrtip',
                'paging'=> false,
                'searching'=> false,
                'order' => [[0, 'desc']],
                "lengthMenu" => [[10, 25, 50, -1], [10, 25, 50, "الكل"]],
                'buttons' => [

                     ['extend' => 'create', 'text' => '<i class="fa fa-plus"></i>الاضافة' , 'className' => 'dt-button buttons-copy buttons-html5 btn btn-default legitRipple'] ,
                    ['extend' => 'export', 'text' => '<i class="fa fa-download"></i>تصدير' , 'className' => 'dt-button buttons-copy buttons-html5 btn btn-default legitRipple'],
                    ['extend' => 'print', 'text' => '<i class="fa fa-print"></i>طباعة' , 'className' => 'dt-button buttons-copy buttons-html5 btn btn-default legitRipple'],
                    ['extend' => 'colvis', 'className' => 'dt-button buttons-collection buttons-colvis btn bg-blue btn-icon legitRipple', 'text' => '<span><i class="icon-three-bars"></i> <span class="caret"></span></span>']

                ],
                'language' => ['url' => asset('ar-datatable.json')],
                'responsive' => true,

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $cols = [
            'name' => ['name' => 'name', 'data' => 'name', 'title' => 'الاسم'],
            'aclass_id' => ['name' => 'aclass_id', 'data' => 'aclass_id', 'title' => 'الفصل'],
            'gender' => ['name' => 'gender', 'data' => 'gender', 'title' => 'النوع'],
            'identifier_number' => ['name' => 'identifier_number', 'data' => 'identifier_number', 'title' => 'رقم الطالب'],
            'status' => ['name' => 'status',  'data' => 'status','title' => 'لحالة'],
            'created_at' => ['name' => 'created_at', 'title' => 'تاريخ الاضافة', 'searchable' => false, 'orderable' => false]
        ];

        $cols['action'] = ['exportable' => false, 'printable' => false, 'searchable' => false, 'orderable' => false, 'title' => 'العمليات'];

        return $cols;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Students_' . date('YmdHis');
    }
}
