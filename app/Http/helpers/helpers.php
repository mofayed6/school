<?php
/**
 * Created by PhpStorm.
 * User: mohamed
 * Date: 14/05/19
 * Time: 12:32
 */



function adminHasPermissions($permission)
{
    if (auth('admin')->check())
        return auth('admin')->user()->rule->permissions->contains('permission', $permission);
    return false;
}

function returnValueNumber($type)
{
    if($type == 'on')
    {
        return 1;
    }else{
        return 0;
    }
}

