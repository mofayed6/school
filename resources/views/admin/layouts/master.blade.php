@include('admin.praical.header')
@include('admin.praical.navbar')
@include('admin.praical.menu')

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">
          @yield('content')
        </div>

    </div>
</div>

@include('admin.praical.footer')
