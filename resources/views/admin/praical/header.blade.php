
    <!DOCTYPE html>
    <html lang="en" dir="rtl">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('page-title')</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{ asset('global_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
        @yield('style')
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="{{ asset('global_assets/js/plugins/loaders/pace.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/core/libraries/jquery.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/core/libraries/bootstrap.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/ui/nicescroll.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/ui/drilldown.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/ui/fab.min.js') }}"></script>
        <!-- /core JS files -->

        <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>

        <!-- Theme JS files -->
        <script src="{{ asset('global_assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>



        <script src="{{ asset('global_assets/js/plugins/notifications/jgrowl.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/pickers/anytime.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/pickers/pickadate/picker.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/pickers/pickadate/picker.date.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/pickers/pickadate/picker.time.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/pickers/pickadate/legacy.js') }}"></script>



        <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js') }}"></script>
        <script src="{{ asset('global_assets/js/demo_pages/datatables_extension_buttons_html5.js') }}"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>



        {{--<script src="{{ asset('assets/js/app.js') }}"></script>--}}
        <script src="{{ asset('global_assets/js/demo_pages/dashboard.js') }}"></script>
        <script src="{{ asset('global_assets/js/demo_pages/form_layouts.js') }}"></script>
        <script src="{{ asset('global_assets/js/demo_pages/picker_date_rtl.js') }}"></script>




        <!-- /theme JS files -->
    </head>

    <body class="navbar-bottom">

    <!-- Page header -->
    <div class="page-header page-header-inverse">



