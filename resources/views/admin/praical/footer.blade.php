<!-- Footer -->
<div class="navbar navbar-default navbar-fixed-bottom footer">
    <ul class="nav navbar-nav visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="footer">
        <div class="navbar-text">
            &copy; 2015. <a href="#" class="navbar-link">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" class="navbar-link" target="_blank">Eugene Kopyov</a>
        </div>

        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="#">About</a></li>
                <li><a href="#">Terms</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- /footer -->
<script src="{{ asset('js/script-2.js') }}"></script>


<script type="text/javascript">


    // $("#resetPassword")[0].reset();


    $(document).on('click', '.destroy', function () {
        var route = $(this).data('route');
        var token = $(this).data('token');
        $.confirm({
            icon: 'glyphicon glyphicon-floppy-remove',
            title: "{{__('devices.are-you-sure')}}",
            text: "{{ __('devices.You-will-not') }}",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButton: "{{__('devices.yes')}}",
            cancelButton: "{{__('devices.cancel')}}",
            closeOnConfirm: false,
            closeOnCancel: false,
            autoClose: 'cancel|6000',
            confirm: function () {
                $.ajax({
                    url: route,
                    type: 'get',
                    data: {_method: 'delete', _token: token},
                    dataType: 'json',
                    success: function (data) {
                        // console.log(data);
                        $("#" + data).parents("tr").remove();

                        swal("{{ __('devices.deleted-successfully') }}!", "", "success");
                    }

                });
            },
        });
    });
</script>

@yield('script')

</body>
</html>
