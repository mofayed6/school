@extends('admin.layouts.master')
@section('page-title','Create New Rule')
@section('breadcrumb')
    <li class="breadcrumb-item active"><a href="{{url('admin/rules')}}">Admin Rules</a></li>
    <li class="breadcrumb-item active">@yield('page-title')</li>
@endsection
@section('content')
    <div class="card">
        <div class="header">
            <div class="row">
                <div class="col-md-6">
                    <h2>Create New Rule </h2>
                </div>
            </div>
        </div>
        <div class="body">
            @include('praical')
            <div class="table-responsive">
                <ul class="nav nav-tabs">
                    @foreach(locales() as $k=>$local)
                        <li class="nav-item"><a class="nav-link {{($k==0)?'active show':''}}" data-toggle="tab" href="#{{ $local->code}}_tab">{{ $local->local_name}}</a></li>
                    @endforeach
                </ul>
                {{Form::model($rule,['action'=>['\Modules\Rules\Http\Controllers\RulesController@update',$rule->id],'method'=>'PATCH'])}}


                <div class="tab-content">
                    @foreach(locales() as $k=>$local)
                        <div class="tab-pane {{($k==0)?'active show':''}}" id="{{ $local->code}}_tab">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name {{ $local->code}} </label>
                                        {{Form::text($local->code.'[name]',$name[$local->code],['class'=>'form-control'])}}
                                    </div>
                                </div>

                            </div>
                        </div>
                    @endforeach
                    <div class="row">
                        @foreach($permissions as $permission)
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="fancy-checkbox">
                                        {{Form::checkbox('permissions[]',$permission->id,null)}}
                                        <span>{{$permission->permission}}</span>
                                    </label>
                                </div>
                            </div>
                        @endforeach

                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success"> Save </button>
                            </div>
                        </div>

                    </div>
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>
@endsection
