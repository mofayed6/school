@extends('admin.layouts.master')
@section('page-title', 'List Students')

@section('content')

    <!-- Column selectors -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">عرض الطلاب</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>


        @include('admin.praical.message')

        <div class="panel-body">
        </div>
        {!! $dataTable->table(['class' => 'table datatable-button-html5-columns' , 'width' => '100%'],true) !!}

    </div>
    <!-- /column selectors -->
@stop


@section('script')
    <script src="{{ asset('/vendor/datatables/buttons.server-side.js') }}"></script>
    {!! $dataTable->scripts() !!}
@endsection
