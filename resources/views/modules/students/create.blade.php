@extends('admin.layouts.master')
@section('page-title', 'Admin Students')

@section('content')
    <!-- Form validation -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">اضافة طالب جديد</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">

             @include('modules.students.form')

        </div>

    </div>


@stop


@section('script')

    <Script>
        $(document).ready(function() {
            $("#years").on('change',function(event){
                var year = $('#years').val();
                console.log(year);
                $.ajax({
                    url: "{{ url('admin/students') }}" + '/' + year +'/class',
                    type: 'GET',
                    dataType: 'json',
                    success: function(res) {
                        $("#aclass_id").append(new Option(res.title, res.id));
                    }
                });

            });
        });


        $("#alpaca-email").alpaca({
            "data": "support",
            "schema": {
                "format": "email"
            }
        });

    </Script>

@endsection
