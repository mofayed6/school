@include('admin.praical.message')

@if(isset($students))


                    {!! Form::model($students,['url'=>['admin/students/'.$students->id.'/update'],'method'=>'PATCH','id'=>'form-horizontal form-validate-jquery ajax','files'=>true]) !!}

                @else

                    {{Form::open(['route'=>'Students.store','files'=>true , 'class'=>'form-horizontal form-validate-jquery ajax'])}}


@endif

                <fieldset class="content-group">
                        <legend class="text-bold"></legend>

                        <!-- Basic text input -->
                        <div class="form-group">
                            <label class="control-label col-lg-3">اسم الطالب <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <input type="text" name="name" class="form-control form-data-input"  placeholder="الاسم">
                                <div class="error validation-error-label">{{ $errors->first('name') }}</div>

                            </div>
                        </div>
                        <!-- /basic text input -->


                        <!-- Input with icons -->
                        <div class="form-group has-feedback">
                            <label class="control-label col-lg-3">رقم الطالب <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <input type="text" name="identifier_number" class="form-control form-data-input"  placeholder="رقم الهوية">
                                <div class="form-control-feedback">
                                    <i class="icon-droplets"></i>
                                </div>
                            </div>
                        </div>
                        <!-- /input with icons -->


                    <!-- Styled file uploader -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">صورة الطالب <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="file" name="image" class="file-styled form-data-input" required="required">
                        </div>
                    </div>
                    <!-- /styled file uploader -->


                        <!-- Input group -->
                        <div class="form-group">
                            <label class="control-label col-lg-3">اسم الدخول<span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="icon-mention"></i></div>
                                    <input type="text" name="username" class="form-control  form-data-input"  >
                                </div>
                            </div>
                        </div>
                        <!-- /input group -->


                        <!-- Password field -->
                        <div class="form-group">
                            <label class="control-label col-lg-3">الرقم السرى <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <input type="password" name="password" id="password" class="form-control form-data-input"  placeholder="اقل عدد للاحرف 5 ">
                            </div>
                        </div>
                        <!-- /password field -->


                        <div class="form-group">
                            <label class="control-label col-lg-3">ولى الامر </label>
                            <div class="col-lg-9">

                                {{Form::select('studentsparent_id',$parents,null,['class'=>'form-control select form-data-input'])}}

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-lg-3"> الصف الدراسى  <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                {{Form::select('years',$years,null,['class'=>'form-control select form-data-input','id'=>'years','placeholder'=>"اختر الصف"])}}
                            </div>
                        </div>


                    <div class="form-group">
                        <label class="control-label col-lg-3"> الفصل <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <select name="aclass_id" id="aclass_id" class="select form-control form-data-input" >

                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-lg-3">النوع  <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            {!! Form::select('gender',['1' => 'ذكر','2'=>'أنثى'],null,['class'=>'select form-control form-data-input']) !!}
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-lg-3">الديانة  <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            {!! Form::select('religon',['مسلم' => 'مسلم','مسيحى'=>'مسيحى'],null,['class'=>'select form-control form-data-input']) !!}
                        </div>
                    </div>


                        <!-- Email field -->
                        <div class="form-group">
                            <label class="control-label col-lg-3">تاريخ الميلاد <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                    <input type="text" name="birthdate" class="form-control daterange-single form-data-input" value="03/18/2013">
                                </div>

                            </div>
                        </div>
                        <!-- /email field -->


                        <!-- Repeat email -->
                        <div class="form-group">
                            <label class="control-label col-lg-3">الموبايل <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <input type="tel" name="mobile" class="form-control form-data-input"  placeholder="">
                            </div>
                        </div>
                        <!-- /repeat email -->


                        <!-- Minimum characters -->
                        <div class="form-group">
                            <label class="control-label col-lg-3">رقم المنزل <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <input type="tel" name="phone" class="form-control form-data-input"  placeholder="">
                            </div>
                        </div>
                        <!-- /minimum characters -->


                        <!-- Maximum characters -->
                        <div class="form-group">
                            <label class="control-label col-lg-3">البريد الالكترونى<span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <input type="email" name="email" class="form-control form-data-input"  placeholder="">
                            </div>
                        </div>
                        <!-- /maximum characters -->



                    <!-- Basic textarea -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">العنوان  <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <textarea rows="5" cols="5" name="address" class="form-control form-data-input"  placeholder="العنوان بالتفصيل"></textarea>
                        </div>
                    </div>
                    <!-- /basic textarea -->



                    <!-- Switch single -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">مصاريف  <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" name="is_expenses" data-on-text="1" data-off-text="0" class="switch form-data-input" >
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /switch single -->


                    <!-- Switch single -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">مصاريف الكتب المدرسية  <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" name="is_books" data-on-text="1" data-off-text="0" class="switch form-data-input" >
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /switch single -->


                    <!-- Switch single -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">  مصاريف الزى المدرسى  <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" name="is_clothes" data-on-text="1" data-off-text="0" class="switch form-data-input" >
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /switch single -->


                    <!-- Switch single -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">     مشترك فى الانشطة    <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" name="is_activites" data-on-text="1" data-off-text="0" class="switch form-data-input"  >
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /switch single -->

                    <!-- Switch single -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">    مشترك فى خط المواصلات  <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="checkbox checkbox-switch">
                                <label>
                                    <input type="checkbox" name="is_bus" data-on-text="1" data-off-text="0" class="switch form-data-input" >
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /switch single -->


                    <div class="form-group">
                        <label class="control-label col-lg-3">  رقم خط المواصلات  <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            {{Form::select('bus_id',$buses,null,['class'=>'form-control select form-data-input','id'=>'bus','placeholder'=>"اختر الكود"])}}
                        </div>
                    </div>





                        <!-- Minimum number -->
                        <div class="form-group">
                            <label class="control-label col-lg-3">الخصم  <span class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <input type="number" name="discount" class="form-control form-data-input"  placeholder="">
                            </div>
                            <span class="help-block">يرجى ادخال النسبة كرقم فقط و بدون %</span>

                        </div>
                        <!-- /minimum number -->


                    <!-- Single styled checkbox -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">محول الى المدرسة <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="transfered_to_here" class="styled form-data-input" value="1" >
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /single styled checkbox -->


                    <!-- Single styled checkbox -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">محول من المدرسة   <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="transfered_from_here" class="styled form-data-input" value="1" >
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /single styled checkbox -->



                    <!-- Maximum number -->
                        <div class="form-group">
                            <label class="control-label col-lg-3">سبب التحويل - من / الى  </label>
                            <div class="col-lg-9">
                                <input type="text" name="transfer_reason" class="form-control form-data-input" >
                            </div>
                        </div>
                        <!-- /maximum number -->


                    <!-- Single styled checkbox -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">  ضمن الطلاب الايتام<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="is_orphan" class="styled form-data-input" value="1 ">
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /single styled checkbox -->


                    <!-- Single styled checkbox -->
                    <div class="form-group">
                        <label class="control-label col-lg-3"> من ابناء العاملين<span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="is_parent_in_school" class="styled form-data-input"  value="1" >
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /single styled checkbox -->



                    <!-- Single styled checkbox -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">  مفعل   <span class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="status" class="styled form-data-input" value="1">
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /single styled checkbox -->




                    </fieldset>

                    <div class="text-right">
                        <button type="reset" class="btn btn-default" id="reset">تحديث <i class="icon-reload-alt position-right"></i></button>
                        <button type="submit" class="btn btn-primary">حفظ <i class="icon-arrow-left13 position-right"></i></button>
                    </div>
                {{ Form::close() }}

        <!-- /form validation -->
