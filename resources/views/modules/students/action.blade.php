<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>

        <ul class="dropdown-menu dropdown-menu-right">

                <li>
                    <a href="{{ route('Students.edit', $id) }}"><i class="icon-pencil5"></i> تعديل</a>
                </li>



                <li>
                    <a class="destroy" id="{{$id}}" data-token="{{ csrf_token() }}" data-route="{{ route('Students.delete', $id) }}" type="button" title="حذف"><i class="icon-bin"></i>حذف</a>
                </li>

        </ul>

    </li>
</ul>
