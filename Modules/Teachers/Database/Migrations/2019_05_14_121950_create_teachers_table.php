<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username');
            $table->string('password');
            $table->date('birthdate');
            $table->text('qualification');
            $table->string('email');
            $table->string('phone');
            $table->string('mobile');
            $table->text('address');
            $table->integer('gender');
            $table->text('other_qualifications');
            $table->string('photo');
            $table->text('experiences');
            $table->text('courses');
            $table->text('other_details');
            $table->unsignedInteger('subject_id');
            $table->unsignedInteger('job_degree_id');
            $table->string('aclasses');
            $table->dateTime('join_date');
            $table->decimal('salary',10,2);
            $table->decimal('incentive',10,2);
            $table->decimal('recompense',10,2);
            $table->string('identifier_number');
            $table->string('religon');
            $table->integer('type');
            $table->tinyInteger('status');
            $table->timestamps();
        });


        Schema::table('teachers', function (Blueprint $table) {
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
            $table->foreign('job_degree_id')->references('id')->on('job_degrees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
