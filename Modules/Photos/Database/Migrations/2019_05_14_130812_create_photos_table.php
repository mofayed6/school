<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('photo');
            $table->unsignedInteger('photoscat_id');
            $table->integer('views');
            $table->integer('all_rates');
            $table->integer('count_rates');
            $table->integer('downloads');
            $table->longText('description');
            $table->longText('comments');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::table('photos', function (Blueprint $table) {
            $table->foreign('photoscat_id')->references('id')->on('photoscats')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
