<?php

namespace Modules\Aclasses\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Students\Entities\Student;
use Modules\Years\Entities\Year;

class Aclass extends Model
{
    protected $fillable = [];

    public function years()
    {
        return $this->belongsTo(Year::class,'year_id');
    }

    public function students()
    {
        return $this->belongsTo(Student::class,'student_id');
    }
}
