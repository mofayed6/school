<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAclassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aclasses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('code');
            $table->unsignedInteger('year_id');
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('teacher_id');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::table('aclasses', function (Blueprint $table) {
            $table->foreign('year_id')->references('id')->on('years')->onDelete('cascade');
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aclasses');
    }
}
