<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homeworks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('aclass_id');
            $table->unsignedInteger('teacher_id');
            $table->string('title');
            $table->text('content');
            $table->string('attachements');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->dateTime('active_date');
            $table->tinyInteger('status');

            $table->timestamps();
        });

        Schema::table('homeworks', function (Blueprint $table) {
            $table->foreign('aclass_id')->references('id')->on('aclasses')->onDelete('cascade');
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homeworks');
    }
}
