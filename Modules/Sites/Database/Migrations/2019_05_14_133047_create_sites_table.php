<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('photo');
            $table->unsignedInteger('sitescat_id');
            $table->integer('views');
            $table->integer('clicks');
            $table->integer('all_rates');
            $table->integer('count_rates');
            $table->text('link');
            $table->text('description');
            $table->longText('comments');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::table('sites', function (Blueprint $table) {
            $table->foreign('sitescat_id')->references('id')->on('sitescats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
