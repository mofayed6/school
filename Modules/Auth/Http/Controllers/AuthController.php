<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{
    public function login()
    {
        if(!auth()->guard('admin')->check())
            return view('admin.auth.login');

        return redirect('admin/login');
    }

    public function dologin(Request $request)
    {

        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:4',
        ]);
       // dd($request->all());

        $remember = request('remember') ? true : false;
        if (!auth()->guard('admin')->attempt(['email'=>request('email'), 'password'=>request('password')],$remember)){
            return redirect('/admin/login')->withErrors(['error' => 'incorrect information login']);
           // return redirect('admin/dashboard');
        }else{
            return redirect('admin/dashboard');
        }
    }

    public function logout()
    {
        auth()->guard('admin')->logout();
        return redirect('/admin/login');

    }
}
