<?php


    Route::group(['prefix' => 'admin'], function () {

        Route::get('login', 'AdminAuth@login')->name('admin.login');
        Route::post('login', 'AdminAuth@dologin');
        $this->get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        $this->post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        $this->get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        $this->post('password/reset', 'ResetPasswordController@reset')->name('password.update');

        Route::group(['middleware' => 'admin:admin'], function () {


            Route::any('logout', 'AdminAuth@logout')->name('doLogout');

            Route::get('/dashboard','\Modules\HomePages\Http\Controllers\HomePagesController@Dashboard')->name('home');

            Route::post('/homeFilter','\Modules\HomePages\Http\Controllers\HomePagesController@homeFilter')->name('homeFilter');
            Route::get('/getOrderHome','\Modules\HomePages\Http\Controllers\HomePagesController@getOrderHome')->name('getOrderHome');
            Route::get('/getSaledHome','\Modules\HomePages\Http\Controllers\HomePagesController@getSaledHome')->name('getSaledHome');
            Route::get('/getOrderProssing','\Modules\HomePages\Http\Controllers\HomePagesController@getOrderProssing')->name('getOrderProssing');
            Route::get('/getOrderDelivered','\Modules\HomePages\Http\Controllers\HomePagesController@getOrderDelivered')->name('getOrderDelivered');
            Route::get('/getOrderDelivering','\Modules\HomePages\Http\Controllers\HomePagesController@getOrderDelivering')->name('getOrderDelivering');
            Route::get('/getOrderCanceled','\Modules\HomePages\Http\Controllers\HomePagesController@getOrderCanceled')->name('getOrderCanceled');
            Route::get('/getOrderPending','\Modules\HomePages\Http\Controllers\HomePagesController@getOrderPending')->name('getOrderPending');
            Route::post('/saleOfWeeks','\Modules\HomePages\Http\Controllers\HomePagesController@saleOfWeeks')->name('saleOfWeeks');
            Route::post('/saleOfDayMonth','\Modules\HomePages\Http\Controllers\HomePagesController@saleOfDayMonth')->name('saleOfDayMonth');
            Route::post('/saleOfDayYear','\Modules\HomePages\Http\Controllers\HomePagesController@saleOfDayYear')->name('saleOfDayYear');
        });



    });


