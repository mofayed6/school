<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('path');
            $table->string('photo');
            $table->unsignedInteger('bookscat_id');
            $table->integer('views');
            $table->integer('downloads');
            $table->integer('all_rates');
            $table->integer('count_rates');
            $table->text('description');
            $table->longText('comments');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::table('books', function (Blueprint $table) {
            //foreign key constrains
            $table->foreign('bookscat_id')->references('id')->on('bookscats')->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
