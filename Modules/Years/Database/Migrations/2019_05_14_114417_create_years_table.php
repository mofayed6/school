<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('years', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('school_level_id');
            $table->integer('code');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::table('years', function (Blueprint $table) {
            $table->foreign('school_level_id')->references('id')->on('school_levels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('years');
    }
}
