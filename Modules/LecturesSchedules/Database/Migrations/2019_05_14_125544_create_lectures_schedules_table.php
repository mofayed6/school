<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturesSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lectures_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('aclass_id');
            $table->unsignedInteger('subject_id');
            $table->integer('day_date');
            $table->integer('lecturenumber');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::table('lectures_schedules', function (Blueprint $table) {
            $table->foreign('aclass_id')->references('id')->on('aclasses')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lectures_schedules');
    }
}
