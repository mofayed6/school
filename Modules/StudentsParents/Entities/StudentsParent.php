<?php

namespace Modules\StudentsParents\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Students\Entities\Student;

class StudentsParent extends Model
{
    protected $fillable = [];

    public function students()
    {
        return $this->hasMany(Student::class);
    }
}
