<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_parents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('gender');
            $table->date('birthdate');
            $table->string('photo');
            $table->string('job');
            $table->string('username');
            $table->string('password');
            $table->string('phone');
            $table->string('email');
            $table->string('mobile');
            $table->string('identifier_id');
            $table->string('religon');
            $table->text('address');
            $table->tinyInteger('is_trustees');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_parents');
    }
}
