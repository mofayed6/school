<div class="tab-content">
    @foreach(locales() as $k=>$local)
        <div class="tab-pane {{($k==0)?'active show':''}}" id="{{ $local->code}}_tab">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Name {{ $local->code}} </label>
                        {{Form::text($local->code.'[name]',null,['class'=>'form-control'])}}
                    </div>
                </div>

            </div>
        </div>
    @endforeach
    <div class="row">
        @foreach($permissions as $permission)
            <div class="col-md-3">
                <div class="form-group">
                    <label class="fancy-checkbox">
                        {{Form::checkbox('permissions[]',$permission->id,null)}}
                        <span>{{$permission->permission}}</span>
                    </label>
                </div>
            </div>
        @endforeach

        <div class="col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-success"> Save </button>
            </div>
        </div>

    </div>
</div>
