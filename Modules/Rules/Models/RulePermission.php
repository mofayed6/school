<?php

namespace Modules\Rules\Models;

use Illuminate\Database\Eloquent\Model;

class RulePermission extends Model
{
    protected $fillable = ['rule_id', 'permission_id'];
}
