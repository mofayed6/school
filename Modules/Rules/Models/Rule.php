<?php

namespace Modules\Rules\Models;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{

    protected $fillable = [];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'rule_permissions', 'rule_id', 'permission_id');
    }
}
