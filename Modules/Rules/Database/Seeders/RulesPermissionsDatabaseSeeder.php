<?php

namespace Modules\Rules\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Rules\Models\RulePermission;

class RulesPermissionsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        RulePermission::create(['rule_id' => 1, 'permission_id' => 1]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 2]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 3]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 4]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 5]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 6]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 7]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 8]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 9]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 10]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 11]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 12]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 13]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 14]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 15]);
        RulePermission::create(['rule_id' => 1, 'permission_id' => 16]);

        // $this->call("OthersTableSeeder");
    }
}
