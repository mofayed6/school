<?php

namespace Modules\Rules\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Rules\Models\Rule;

class RulesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $rule = Rule::create(['id' => 1]);
        $rule->translateAttributes([
            'ar' => [
                'name' => 'المدير الأساسي'
            ],
            'en' => [
                'name' => 'Super admin'
            ]
        ]);

        $this->call(PermissionsDatabaseSeeder::class);
        $this->call(RulesPermissionsDatabaseSeeder::class);
    }
}
