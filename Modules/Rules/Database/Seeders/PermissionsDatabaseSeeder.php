<?php

namespace Modules\Rules\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Rules\Models\Permission;

class PermissionsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::create(['permission' => 'rules']);
        Permission::create(['permission' => 'admins']);
        Permission::create(['permission' => 'locations']);
        Permission::create(['permission' => 'branches']);
        Permission::create(['permission' => 'categories']);
        Permission::create(['permission' => 'attributes']);
        Permission::create(['permission' => 'classifications']);
        Permission::create(['permission' => 'units']);
        Permission::create(['permission' => 'identifiers']);
        Permission::create(['permission' => 'products']);
        Permission::create(['permission' => 'user']);
        Permission::create(['permission' => 'coupons']);
        Permission::create(['permission' => 'pages']);
        Permission::create(['permission' => 'orders']);
        Permission::create(['permission' => 'dashboard']);
        Permission::create(['permission' => 'home-pages']);
        
        // $this->call("OthersTableSeeder");
    }
}
