<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('photo');
            $table->string('path');
            $table->string('youtube');
            $table->unsignedInteger('videoscat_id');
            $table->integer('views');
            $table->integer('all_rates');
            $table->integer('downloads');
            $table->integer('count_rates');
            $table->text('link');
            $table->text('description');
            $table->longText('comments');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::table('videos', function (Blueprint $table) {
            $table->foreign('videoscat_id')->references('id')->on('videoscats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
