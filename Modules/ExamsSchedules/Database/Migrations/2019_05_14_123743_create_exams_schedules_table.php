<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->date('exam_date');
            $table->unsignedInteger('year_id');
            $table->unsignedInteger('subject_id');
            $table->dateTime('from_date');
            $table->dateTime('to_date');
            $table->string('place');
            $table->text('details');
            $table->enum('type',['shafawy','nazary','3amaly']);
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::table('exams_schedules', function (Blueprint $table) {
            $table->foreign('year_id')->references('id')->on('years')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams_schedules');
    }
}
