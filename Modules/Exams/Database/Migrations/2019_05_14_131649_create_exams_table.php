<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('aclass_id');
            $table->unsignedInteger('subject_id');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('time_to_end');
            $table->tinyInteger('auto_correct');
            $table->tinyInteger('status');

            $table->timestamps();
        });

        Schema::table('exams', function (Blueprint $table) {
            $table->foreign('aclass_id')->references('id')->on('aclasses')->onDelete('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
