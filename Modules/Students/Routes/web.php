<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware('admin:admin')->prefix('admin/students')->group(function () {


    Route::get('/', [
        'uses' => 'StudentsController@index',
        'as' => 'Students.index'
    ]);

    Route::get('/create', [
        'uses' => 'StudentsController@create',
        'as' => 'Students.create'
    ]);


    Route::post('/', [
        'uses' => 'StudentsController@store',
        'as' => 'Students.store'
    ]);


    Route::get('/{student}/show', [
        'uses' => 'StudentsController@shows',
        'as' => 'Students.shows'
    ]);

    Route::get('/{student}/edit', [
        'uses' => 'StudentsController@edit',
        'as' => 'Students.edit'
    ]);

    Route::patch('/{student}/update', [
        'uses' => 'StudentsController@update',
        'as' => 'Students.update'
    ]);

    Route::get('/{student}/delete', [
        'uses' => 'StudentsController@delete',
        'as' => 'Students.delete'
    ]);


    Route::get('/{years}/class', [
        'uses' => 'StudentsController@getClass',
    ]);


});