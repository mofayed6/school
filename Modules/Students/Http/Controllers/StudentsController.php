<?php

namespace Modules\Students\Http\Controllers;

use App\DataTables\StudentsDataTable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Aclasses\Entities\Aclass;
use Modules\Buses\Entities\Bus;
use Modules\Students\Entities\Student;
use Modules\Students\Http\Requests\StudentRequest;
use Modules\StudentsParents\Entities\StudentsParent;
use Modules\Years\Entities\Year;
use Carbon\Carbon;

class StudentsController extends Controller
{

    /**
     * @param StudentsDataTable $dataTable
     * @return mixed
     */
    public function index(StudentsDataTable $dataTable)
    {

        return $dataTable->render('students::index');

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $parents = StudentsParent::pluck('name','id')->toArray();
        $years   = Year::pluck('title','id')->toArray();
        $buses   = Bus::pluck('code','id')->toArray();
        return view('students::create',compact('parents','years','buses'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(StudentRequest $request)
    {

        $newStudent = new Student();
        $newStudent->name                    = $request->name;
        $newStudent->aclass_id               = $request->aclass_id;
        $newStudent->bus_id                  = $request->bus_id;
        $newStudent->studentsparent_id       = $request->studentsparent_id;
        $newStudent->gender                  = $request->gender;
        $newStudent->birthdate               = Carbon::parse($request->birthdate)->format('Y-m-d H:i:s');
        $newStudent->mobile                  = $request->mobile;
        $newStudent->phone                   = $request->phone;
        $newStudent->photo                   = $request->image->store('students');
        $newStudent->username                = $request->username;
        $newStudent->password                = bcrypt($request->password);
        $newStudent->email                   = $request->email;
        $newStudent->identifier_number       = $request->identifier_number;
        $newStudent->religon                 = $request->religon;
        $newStudent->address                 = $request->address;
        $newStudent->is_orphan               = $request->is_orphan;
        $newStudent->is_parent_in_school     = $request->is_parent_in_school;
        $newStudent->transfered_to_here      = $request->transfered_to_here;
        $newStudent->transfered_from_here    = $request->transfered_from_here;
        $newStudent->transfer_reason         = $request->transfer_reason;
        $newStudent->discount                = $request->discount;
        $newStudent->status                  = $request->status;
        $newStudent->is_bus                  = returnValueNumber($request->is_bus);
        $newStudent->is_activites            = returnValueNumber($request->is_activites);
        $newStudent->is_books                = returnValueNumber($request->is_books);
        $newStudent->is_clothes              = returnValueNumber($request->is_clothes);
        $newStudent->is_expenses             = returnValueNumber($request->is_expenses);

        $newStudent->save();


        if ($request->ajax()) {
            return response()->json([
                'requestStatus' => true,
                'message' => 'تم الاضافة بنجاح']);
        }

        return redirect('students')->with('error', 'حدث خطأ خلال الاضافة الرجاء المحاولة مرة ثانية');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('students::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $parents = StudentsParent::pluck('name','id')->toArray();
        $years   = Year::pluck('title','id')->toArray();
        $buses   = Bus::pluck('code','id')->toArray();
        return view('students::edit',compact('parents','years','buses'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * get class student when send id years
     * @param int $years
     * @return object
     */
    public function getClass(int $years) : object
    {
        $class = Aclass::where('year_id',$years)->first();
        return $class;
    }
}
