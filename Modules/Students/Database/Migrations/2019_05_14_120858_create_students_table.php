<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('aclass_id');
            $table->unsignedInteger('bus_id');
            $table->unsignedInteger('studentsparent_id');
            $table->integer('gender');
            $table->date('birthdate');
            $table->string('photo');
            $table->string('mobile');
            $table->string('phone');
            $table->string('username');
            $table->string('password');
            $table->string('email');
            $table->string('identifier_number');
            $table->string('religon');
            $table->text('address');
            $table->tinyInteger('is_bus');
            $table->tinyInteger('is_activites');
            $table->tinyInteger('is_books');
            $table->tinyInteger('is_clothes');
            $table->tinyInteger('is_expenses');
            $table->tinyInteger('is_trustees');
            $table->tinyInteger('is_orphan');
            $table->tinyInteger('is_parent_in_school');
            $table->tinyInteger('transfered_to_here');
            $table->tinyInteger('transfered_from_here');
            $table->string('transfer_reason');
            $table->decimal('discount',10,2);
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::table('students', function (Blueprint $table) {
            $table->foreign('bus_id')->references('id')->on('buses')->onDelete('cascade');
            $table->foreign('studentsparent_id')->references('id')->on('students_parents')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
