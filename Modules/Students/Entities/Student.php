<?php

namespace Modules\Students\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\StudentsParents\Entities\StudentsParent;

class Student extends Model
{
    protected $fillable = [];

    public function studentsParent()
    {
        return $this->belongsTo(StudentsParent::class);
    }
}

