<?php

namespace Modules\HomePages\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class HomePagesController extends Controller
{
    public function Dashboard()
    {
        return view('admin.home');
    }

}
